from django.contrib.admin.views.decorators import staff_member_required
from django.db import IntegrityError
from ninja import NinjaAPI, Schema

from cards.models import UnverifiedCardLevelData

# https://django-ninja.dev/guides/api-docs/
api = NinjaAPI(docs_decorator=staff_member_required, title="Hamster API")


class LevelIn(Schema):
    card_id: int
    level: int
    price: int
    profit_per_hour: int


class LevelOut(Schema):
    id: int


class Message(Schema):
    message: str


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


# https://django-ninja.dev/guides/response/#multiple-response-schemas
@api.post('/add-level', response={200: LevelOut, 404: Message}, tags=['cards'])
def add_level(request, payload: LevelIn):
    """Добавление уровня карточки"""
    data: dict = payload.dict()
    data['user_data'] = {'ip': get_client_ip(request)}

    try:
        new_data = UnverifiedCardLevelData.objects.create(**data)
        return LevelOut(id=new_data.id)
    except IntegrityError as e:
        return 404, Message(message='Card not found')
