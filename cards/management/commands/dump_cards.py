import json
from functools import lru_cache

from django.core.management import BaseCommand

from cards.models import Category, Card, Level
from hamster.settings import BASE_DIR

symbols = {
    1: '',
    1e3: 'K',
    1e6: 'M',
}


@lru_cache
def num_format(num: int) -> str:
    result = str(num)
    for digit, symbol in symbols.items():
        if num >= digit:
            res = num / digit
            left, right = str(res).split('.')
            right = right.rstrip('0')
            current_result = f'{left},{right}' if right else f'{left}'
            result = f'{current_result}{symbol}'
    return result


class Command(BaseCommand):
    def handle(self, *args, **options):
        levels = list(Level.objects.values('id', 'card__id', 'level', 'price', 'profit_per_hour', 'efficiency_factor'))
        data = {
            'categories': list(Category.objects.values('id', 'name')),
            'cards': list(Card.objects.values('id', 'category__id', 'name', 'active')),
            'levels': [
                {
                    'id': lvl.get('id'),
                    'card__id': lvl.get('card__id'),
                    'level': lvl.get('level'),
                    'price': num_format(lvl.get('price')),
                    'efficiency_factor': round(lvl.get('efficiency_factor') or 0, 2),
                    'profit_per_hour': num_format(lvl.get('profit_per_hour'))
                } for lvl in levels]
        }
        # ver2
        # data = {}
        # for category in Category.objects.all():
        #     data[category.id] = {'name': category.name, 'cards': []}
        #
        #     for card in category.cards.all():
        #         card_data = {
        #             'id': card.id,
        #             'name': card.name,
        #             'active': card.active,
        #             'levels': [level for level in
        #                        card.levels.values('level', 'price', 'profit_per_hour', 'efficiency_factor')]
        #         }
        #         data[category.id]['cards'].append(card_data)

        data_dir = BASE_DIR / 'hamster-data/'
        data_dir.mkdir(parents=True, exist_ok=True)

        with open(data_dir / 'data.json', 'w') as f:
            json.dump(data, f)

        print(f'cards dumped {data_dir}')
