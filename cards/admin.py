from admin_auto_filters.filters import AutocompleteFilter
from django.contrib import admin, messages
from django.core.management import call_command
from django.db import models
from django.http import HttpResponseRedirect
from django.urls import path
from django_json_widget.widgets import JSONEditorWidget

from cards.models import Card, Category, Level, UnverifiedCardLevelData


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ('name',)


class LevelInline(admin.TabularInline):
    model = Level
    extra = 1

    def get_queryset(self, request):
        qs = super().get_queryset(request).order_by('level')
        return qs


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ('name', 'category',)
    list_filter = ('category',)
    search_fields = ('name',)
    autocomplete_fields = ('category',)
    inlines = (LevelInline,)


class CommandAdmin(admin.ModelAdmin):
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('dump-cards/', self.dump_cards),
        ]
        return my_urls + urls

    @staticmethod
    def dump_cards(request):
        call_command('dump_cards')
        return HttpResponseRedirect('../')


class CardFilter(AutocompleteFilter):
    title = 'Card'
    field_name = 'card'


@admin.register(Level)
class LevelAdmin(CommandAdmin):
    change_list_template = 'cards/levels_changelist.html'

    autocomplete_fields = ('card',)
    list_display = ('card', 'level', 'price', 'profit_per_hour', 'efficiency_factor')
    list_filter = ('card__category', CardFilter, 'level')
    search_fields = ('card__name',)
    readonly_fields = ('efficiency_factor',)


def activate_card_levels(modeladmin, request, queryset):
    new_levels = [Level(
        card=user_data.card, level=user_data.level, price=user_data.price,
        profit_per_hour=user_data.profit_per_hour
    ) for user_data in queryset]
    Level.objects.bulk_create(new_levels, ignore_conflicts=True)

    queryset.update(passed_moderation=True)
    call_command('dump_cards')
    messages.add_message(request, messages.INFO, "Activated card levels saved to file.")


activate_card_levels.short_description = 'Activate selected card levels'


@admin.register(UnverifiedCardLevelData)
class UnverifiedCardLevelDataAdmin(admin.ModelAdmin):
    list_filter = (CardFilter, 'passed_moderation', 'level',)
    fields = ('card', 'level', ('price', 'profit_per_hour'), ('created_at', 'updated_at'), 'user_data',)
    formfield_overrides = {
        models.JSONField: {"widget": JSONEditorWidget},
    }
    list_display = ('card', 'level', 'price', 'profit_per_hour', 'passed_moderation', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    actions = (activate_card_levels,)
