from django.contrib.auth.models import User
from django.db import models
from django.db.models import F, JSONField

from cards.base_models import BaseModel


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ('id', 'name')

    def __str__(self):
        return self.name


class Card(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='cards')
    name = models.CharField(max_length=100, unique=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Cards'
        unique_together = (('category', 'name'),)
        ordering = ('id', 'name')

    def __str__(self):
        return f'{self.category}.{self.name}'


class Level(models.Model):
    card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='levels')
    level = models.PositiveSmallIntegerField(default=0)
    price = models.PositiveIntegerField(blank=True, null=True)
    profit_per_hour = models.PositiveIntegerField(blank=True, null=True)
    efficiency_factor = models.GeneratedField(expression=F('price') * 1.0 / F('profit_per_hour'),
                                              output_field=models.FloatField(), db_persist=True)

    class Meta:
        verbose_name_plural = 'Levels'
        unique_together = ('card', 'level')

    def __str__(self):
        return f'{self.card} {self.level} lvl.'


class UnverifiedCardLevelData(BaseModel):
    user_data = JSONField(blank=True, null=True, default=dict)
    card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='unverified_cards')
    level = models.PositiveIntegerField()
    price = models.PositiveIntegerField(blank=True, null=True)
    profit_per_hour = models.PositiveIntegerField(blank=True, null=True)
    passed_moderation = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Unverified Card Levels'

    def __str__(self):
        return f'{self.card} {self.level} lvl.'
