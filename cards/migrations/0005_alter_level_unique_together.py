# Generated by Django 5.0.6 on 2024-05-25 19:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("cards", "0004_alter_card_category"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="level",
            unique_together={("card", "level")},
        ),
    ]
